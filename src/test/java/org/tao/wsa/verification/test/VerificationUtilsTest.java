package org.tao.wsa.verification.test;

import com.tao.wsa.verification.model.cognalys.CognalysAcknowledgementOutput;
import com.tao.wsa.verification.model.cognalys.CognalysSendPayload;
import com.tao.wsa.verification.model.cognalys.CognalysVerificationOutput;
import com.tao.wsa.verification.model.cognalys.CognalysVerifyPayload;
import com.tao.wsa.verification.phoneapiimpl.CognalysVerificationUtil;

public class VerificationUtilsTest {

	public static void main(String[] args) {
		CognalysVerificationUtil testUtil = new CognalysVerificationUtil();
		
		// Test for send OTP
		CognalysAcknowledgementOutput ack = new CognalysAcknowledgementOutput();
		CognalysSendPayload sendPayload = new CognalysSendPayload();
		sendPayload.setPhoneNumber("+919886796973");
		ack = (CognalysAcknowledgementOutput) testUtil.sendOTP(sendPayload);
		System.out.println("Send Test complete "+ack.getStatus());
		
		//Test for verify OTP
		CognalysVerificationOutput ver = new CognalysVerificationOutput();
		CognalysVerifyPayload verPayload = new CognalysVerifyPayload();
		verPayload.setKeymatch("23890e5cf28e472699720a7");
		verPayload.setOtp("7742284609");
		ver = (CognalysVerificationOutput)testUtil.verifyOTP(verPayload);
		System.out.println("Verify Test complete "+ver.isVerified());

	}

}
