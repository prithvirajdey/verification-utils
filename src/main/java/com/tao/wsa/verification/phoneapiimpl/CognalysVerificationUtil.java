package com.tao.wsa.verification.phoneapiimpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.tao.wsa.verification.client.NetClient;
import com.tao.wsa.verification.model.AcknowledgementOutput;
import com.tao.wsa.verification.model.SendPayload;
import com.tao.wsa.verification.model.VerificationOutput;
import com.tao.wsa.verification.model.VerifyPayload;
import com.tao.wsa.verification.model.cognalys.CognalysAcknowledgementOutput;
import com.tao.wsa.verification.model.cognalys.CognalysSendPayload;
import com.tao.wsa.verification.model.cognalys.CognalysVerificationOutput;
import com.tao.wsa.verification.model.cognalys.CognalysVerifyPayload;
import com.tao.wsa.verification.phoneapi.PhoneVerifyUtil;

public class CognalysVerificationUtil implements PhoneVerifyUtil {

	@Override
	public VerificationOutput verifyOTP(VerifyPayload payload) {
		CognalysVerifyPayload specificPayload = (CognalysVerifyPayload) payload;
		// Read app id and access token from properties file
		Properties prop = new Properties();
		InputStream input = null;
		String url = null;
		CognalysVerificationOutput output = new CognalysVerificationOutput();
		// System.out.println(new File(".").getAbsolutePath());

		try {
			input = CognalysVerificationUtil.class.getClassLoader()
					.getResourceAsStream("verification_config.properties");
			// input = new FileInputStream("verification_config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			url = prop.getProperty("baseConfirmURL");
			specificPayload.setAppId(prop.getProperty("appId"));
			specificPayload.setAccessToken(prop.getProperty("accessToken"));
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// From the GET request URL
		StringBuffer reqURL = new StringBuffer();
		reqURL.append(url);
		reqURL.append("?app_id=" + specificPayload.getAppId());
		reqURL.append("&access_token=" + specificPayload.getAccessToken());
		reqURL.append("&keymatch=" + specificPayload.getKeymatch());
		reqURL.append("&otp=" + specificPayload.getOtp());

		HttpResponse<JsonNode> serviceOutput = NetClient.callCognalysGetService(reqURL.toString());

		// Call the get service
		System.out.println("Response from service " + serviceOutput.getBody().toString());

		if (!serviceOutput.getBody().getObject().isNull("errors")){
			output.setVerified(false);
			output.setMessage(serviceOutput.getBody().getObject().getJSONObject("errors").toString());
		}
		else {
			if(!serviceOutput.getBody().getObject().isNull("codes")){
				output.setVerified(false);
				output.setMessage(serviceOutput.getBody().getObject().getJSONObject("codes").toString());
			}
			else{
			output.setPhoneNumber(serviceOutput.getBody().getObject().getString("mobile"));
			output.setMessage(serviceOutput.getBody().getObject().getString("message"));
			output.setVerified(true);
			}
		}

		return output;
	}

	@Override
	public AcknowledgementOutput sendOTP(SendPayload payload) {
		CognalysSendPayload specificPayload = (CognalysSendPayload) payload;
		// Read app id and access token from properties file
		Properties prop = new Properties();
		InputStream input = null;
		String url = null;
		CognalysAcknowledgementOutput output = new CognalysAcknowledgementOutput();
		// System.out.println(new File(".").getAbsolutePath());

		try {
			input = CognalysVerificationUtil.class.getClassLoader()
					.getResourceAsStream("verification_config.properties");
			// input = new FileInputStream("verification_config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			url = prop.getProperty("baseURL");
			specificPayload.setAppId(prop.getProperty("appId"));
			specificPayload.setAccessToken(prop.getProperty("accessToken"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// From the GET request URL
		StringBuffer reqURL = new StringBuffer();
		reqURL.append(url);
		reqURL.append("?app_id=" + specificPayload.getAppId());
		reqURL.append("&access_token=" + specificPayload.getAccessToken());
		reqURL.append("&mobile=" + specificPayload.getPhoneNumber());

		HttpResponse<JsonNode> serviceOutput = NetClient.callCognalysGetService(reqURL.toString());

		// Call the get service
		System.out.println("Response from service " + serviceOutput.getBody().toString());

		if (!serviceOutput.getBody().getObject().isNull("errors"))
			output.setStatus("failure");
		else {
			if(!serviceOutput.getBody().getObject().isNull("codes")){
				output.setStatus("failure");
			}
			else{
			output.setPhoneNumber(serviceOutput.getBody().getObject().getString("mobile"));
			output.setKeyMatch(serviceOutput.getBody().getObject().getString("keymatch"));
			output.setOtpStart(serviceOutput.getBody().getObject().getString("otp_start"));
			output.setStatus("success");
			}
		}

		return output;
	}

}
