package com.tao.wsa.verification.model.cognalys;

import com.tao.wsa.verification.model.SendPayload;

public class CognalysSendPayload extends SendPayload{
	
	private String appId;
	private String accessToken;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	

}
