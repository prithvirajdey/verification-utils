package com.tao.wsa.verification.model;

import java.util.ArrayList;

public class AcknowledgementOutput {
	
	private String status;
	private String phoneNumber;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	

}
