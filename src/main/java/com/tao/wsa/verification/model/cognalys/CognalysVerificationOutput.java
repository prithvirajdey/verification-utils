package com.tao.wsa.verification.model.cognalys;

import com.tao.wsa.verification.model.VerificationOutput;

public class CognalysVerificationOutput extends VerificationOutput{
	
	private String message;
	private String otpStart;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getOtpStart() {
		return otpStart;
	}
	public void setOtpStart(String otpStart) {
		this.otpStart = otpStart;
	}
	
	

}
