package com.tao.wsa.verification.model.cognalys;

import com.tao.wsa.verification.model.VerifyPayload;

public class CognalysVerifyPayload extends VerifyPayload{
	
	private String keymatch;
	private String otp;
	private String appId;
	private String accessToken;
	
	
	public String getKeymatch() {
		return keymatch;
	}
	public void setKeymatch(String keymatch) {
		this.keymatch = keymatch;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	

}
