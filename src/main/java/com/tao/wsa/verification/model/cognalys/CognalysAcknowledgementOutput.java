package com.tao.wsa.verification.model.cognalys;

import java.util.ArrayList;

import com.tao.wsa.verification.model.AcknowledgementOutput;

public class CognalysAcknowledgementOutput extends AcknowledgementOutput{
	
	private String keyMatch;
	private String otpStart;
	private ArrayList<String> codes;
	public String getKeyMatch() {
		return keyMatch;
	}
	public void setKeyMatch(String keyMatch) {
		this.keyMatch = keyMatch;
	}
	public String getOtpStart() {
		return otpStart;
	}
	public void setOtpStart(String otpStart) {
		this.otpStart = otpStart;
	}
	public ArrayList<String> getCodes() {
		return codes;
	}
	public void setCodes(ArrayList<String> codes) {
		this.codes = codes;
	}
	
	

}
