package com.tao.wsa.verification.phoneapi;

import com.tao.wsa.verification.model.AcknowledgementOutput;
import com.tao.wsa.verification.model.SendPayload;
import com.tao.wsa.verification.model.VerificationOutput;
import com.tao.wsa.verification.model.VerifyPayload;

public interface PhoneVerifyUtil {
	
	public VerificationOutput verifyOTP(VerifyPayload payload);
	public AcknowledgementOutput sendOTP(SendPayload payload);

}
