package com.tao.wsa.verification.client;

import java.io.IOException;
import java.net.MalformedURLException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class NetClient {

	public static HttpResponse<JsonNode> callCognalysGetService(String urlString){

		HttpResponse<JsonNode> response = null;
		  try {
			response = Unirest.get(urlString).asJson();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		// response.getBody().getObject().getJSONObject("errors").getString("500");
		return response;


	}

}
